package pl.codementors.zoo;

import pl.codementors.zoo.animals.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Application main class.
 *
 * @author psysiu
 */
public class ZooMain {

    /**
     * Application starting method.
     *
     * @param args Application starting params.
     */
    public static void main(String[] args) {


        //List<Animal> animals = new ArrayList<>();
        Map<Owner, List<Animal>> ownersAnimals = new TreeMap<>();


        try (InputStream is = ZooMain.class.getResourceAsStream("/pl/codementors/zoo/input/animals.txt");
             Scanner scanner = new Scanner(is);) {
            while (scanner.hasNext()) {
                String animalOwner = scanner.next();
                String etype = scanner.next();
                System.out.println(etype);
                Owner.Type oType = Owner.Type.valueOf(etype);
                Owner owner = new Owner(animalOwner,oType);

                if (!ownersAnimals.containsKey(owner)) {

                    List<Animal> animalList = new ArrayList<>();
                    ownersAnimals.put(owner, animalList);
                }

                String type = scanner.next();
                String name = scanner.next();
                String color = scanner.next();


                switch (type) {
                    case "Bird": {
                        ownersAnimals.get(owner).add(new Bird(name, color));
                        break;
                    }
                    case "Lizard": {
                        ownersAnimals.get(owner).add(new Lizard(name, color));
                        break;
                    }
                    case "Mammal": {
                        ownersAnimals.get(owner).add(new Mammal(name, color));
                        break;
                    }
                    case "Dog": {
                        ownersAnimals.get(owner).add(new Dog(name, color));
                        break;
                    }
                    case "Crow": {
                        ownersAnimals.get(owner).add(new Crow(name, color));
                        break;
                    }
                }

            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }

        //Collections.sort(animals,new Sorter());
        //Set<Animal> animalsSet = new TreeSet<>(new Sorter());
        //animalsSet.addAll(animals);
//        Set<Mammal> mammalSet = new HashSet<>();
//        List<Bird> birdList = new ArrayList<>();
//        Collection<Lizard> lizardCollection = new TreeSet<>();
//
//        for (Animal i : animals){
//            if (i instanceof Mammal){
//                mammalSet.add((Mammal)i);
//            }
//            if (i instanceof Bird){
//                birdList.add((Bird)i);
//            }
//            if (i instanceof Lizard){
//                lizardCollection.add((Lizard)i);
//            }
//        }
//        //printAnimals(animals);
//        printAll(mammalSet);
//        printAll(birdList);
//        printAll(lizardCollection);
        printMap(ownersAnimals);
    }

    public static void printAnimals(Collection<Animal> animals) {

        int counter = 0;
        for (Animal animal : animals) {
            //System.out.println("HashCode: " + animal.hashCode());
            System.out.println(animal);
            System.out.println("--------------------------------");
            counter++;
        }
        System.out.println("Total: " + counter);
    }

    public static void printAll(Collection<? extends Animal> collection) {
        int counter = 0;
        System.out.println(collection.getClass().getSimpleName());
        for (Animal animal : collection) {
            System.out.println(animal);
            System.out.println("--------------------------------");
            counter++;
        }
        System.out.println("Total: " + counter);
    }

    public static void printMap(Map<Owner, List<Animal>> map) {
        int counter = 0;
        //System.out.println(collection.getClass().getSimpleName());
        for (Owner key : map.keySet()) {
            System.out.println(key.getName());
            System.out.println(key.getType());
            System.out.println(map.get(key));
            System.out.println("--------------------------------");
            counter++;
        }
        System.out.println("Total: " + counter);
    }

}
