package pl.codementors.zoo.animals;

import java.util.Comparator;

/**
 * Created by sit0 on 14.06.17.
 */
public class Owner implements Comparable<Owner>{

    private String name;

    private Type type;

    public Owner(String name,Type type) {
        this.name = name;
        this.type = type;
    }



    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Owner owner) {
        int ret = getName().compareTo(owner.getName());
        if (ret == 0){
            return getClass().getCanonicalName().compareTo(owner.getClass().getCanonicalName());
        }
        return ret;
    }

    public enum Type {

        BREEDER,
        COLLECTOR;

    }
}
