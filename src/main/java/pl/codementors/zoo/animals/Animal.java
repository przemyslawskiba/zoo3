package pl.codementors.zoo.animals;

/**
 * Just an animal.
 *
 * @author psysiu
 */
public class Animal implements Comparable<Animal> {

    /**
     * Animal name.
     */
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Animal-> "+"Name: " + name;
    }

    @Override
    public int compareTo(Animal animal) {
        int ret = getName().compareTo(animal.getName());
        if (ret == 0){
            return getClass().getCanonicalName().compareTo(animal.getClass().getCanonicalName());
        }
        return ret;
    }

    @Override
    public boolean equals(Object animal) {

        if (this == animal){
            return true;
        }
        if (!(animal instanceof Animal)){
            return false;
        }
        Animal animal2 = (Animal) animal;
        return getClass().getSimpleName().equals(animal.getClass().getSimpleName()) && getName().equals(animal2.getName());

    }

    @Override
    public int hashCode() {
        if(getName() == null){
            return 0;
        }
        return (getName() + getClass().getSimpleName()).hashCode();
    }
}
