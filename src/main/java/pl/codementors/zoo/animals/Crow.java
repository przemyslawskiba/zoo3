package pl.codementors.zoo.animals;

/**
 * Created by sit0 on 13.06.17.
 */
public class Crow extends Bird {

    public Crow(String name, String featherColor) {
        super(name, featherColor);
    }

    @Override
    public String toString() {
        return "Crow-> " + super.toString();
    }

    @Override
    public boolean equals(Object animal) {

        if (this == animal){
            return true;
        }
        if (!(animal instanceof Crow)){
            return false;
        }
        Crow animal2 = (Crow) animal;
        return getFeatherColor().equals(animal2.getFeatherColor()) && getName().equals(animal2.getName());

    }

    @Override
    public int hashCode() {
        if(getName() == null){
            return 0;
        }
        return (getName() + getClass().getSimpleName()).hashCode();
    }
}
