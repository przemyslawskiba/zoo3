package pl.codementors.zoo.animals;

/**
 * Lizard with some scales.
 *
 * @author psysiu
 */
public class Lizard extends Animal {

    /**
     * Color of the animal scales.
     */
    private String scalesColor;

    public Lizard(String name, String scalesColor) {
        super(name);
        this.scalesColor = scalesColor;
    }

    @Override
    public String toString() {
        return "Lizard-> " + "Color: " + scalesColor + " " + super.toString();
    }

    public String getScalesColor() {
        return scalesColor;
    }

    public void setScalesColor(String scalesColor) {
        this.scalesColor = scalesColor;
    }

    @Override
    public int compareTo(Animal animal) {
        int ret = super.compareTo(animal);
        if (ret == 0){
            if (animal instanceof Lizard){
                return getScalesColor().compareTo(((Lizard) animal).getScalesColor());
            }
        }
        return getName().compareTo(animal.getName());
    }

    @Override
    public boolean equals(Object animal) {

        if (this == animal){
            return true;
        }
        if (!(animal instanceof Lizard)){
            return false;
        }
        Lizard animal2 = (Lizard) animal;
        return getScalesColor().equals(animal2.getScalesColor()) && getName().equals(animal2.getName());

    }

    @Override
    public int hashCode() {
        if(getName() == null){
            return 0;
        }
        return (getName() + getClass().getSimpleName()).hashCode();
    }
}
