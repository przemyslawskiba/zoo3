package pl.codementors.zoo.animals;

/**
 * Just a mammal with some fur.
 *
 * @author psysiu
 */
public class Mammal extends Animal {

    /**
     * Color of the animal fur.
     */
    private String furColor;

    public Mammal(String name, String furColor) {
        super(name);
        this.furColor = furColor;
    }

    @Override
    public String toString() {
        return "Mammal-> " + "Color: " + furColor + " " + super.toString();
    }

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }

    @Override
    public int compareTo(Animal animal) {
        int ret = super.compareTo(animal);
        if (ret == 0){
            if (animal instanceof Bird){
                return getFurColor().compareTo(((Mammal) animal).getFurColor());
            }
        }
        return getName().compareTo(animal.getName());
    }


    @Override
    public boolean equals(Object animal) {

        if (this == animal){
            return true;
        }
        if (!(animal instanceof Mammal)){
            return false;
        }
        Mammal animal2 = (Mammal) animal;
        return getFurColor().equals(animal2.getFurColor()) && getName().equals(animal2.getName());

    }

    @Override
    public int hashCode() {
        if(getName() == null){
            return 0;
        }
        return (getName() + getClass().getSimpleName()).hashCode();
    }
}
