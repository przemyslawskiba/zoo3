package pl.codementors.zoo.animals;

/**
 * Just a bird with feathers.
 *
 * @author psysiu
 */
public class Bird extends Animal {

    /**
     * Color of the bird feathers.
     */
    private String featherColor;

    public Bird(String name, String featherColor) {
        super(name);
        this.featherColor = featherColor;
    }

    @Override
    public String toString() {
        return "Bird -> " + "Color: " + featherColor + " " + super.toString();
    }

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }

    @Override
    public int compareTo(Animal animal) {
        int ret = super.compareTo(animal);
        if (ret == 0){
            if (animal instanceof Bird){
                return getFeatherColor().compareTo(((Bird) animal).getFeatherColor());
            }
        }
        return getName().compareTo(animal.getName());
    }

    @Override
    public boolean equals(Object animal) {

        if (this == animal){
            return true;
        }
        if (!(animal instanceof Bird)){
            return false;
        }
        Bird animal2 = (Bird) animal;
        return getFeatherColor().equals(animal2.featherColor) && getName().equals(animal2.getName());

    }

    @Override
    public int hashCode() {
        if(getName() == null){
            return 0;
        }
        return (getName() + getClass().getSimpleName()).hashCode();
    }
}
