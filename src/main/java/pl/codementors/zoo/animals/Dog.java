package pl.codementors.zoo.animals;

/**
 * Created by sit0 on 13.06.17.
 */
public class Dog extends Mammal {

    public Dog(String name, String furColor) {
        super(name, furColor);
    }

    @Override
    public String toString() {
        return "Dog-> " + super.toString();
    }

    @Override
    public boolean equals(Object animal) {

        if (this == animal){
            return true;
        }
        if (!(animal instanceof Dog)){
            return false;
        }
        Dog animal2 = (Dog) animal;
        return getFurColor().equals(animal2.getFurColor()) && getName().equals(animal2.getName());

    }

    @Override
    public int hashCode() {
        if(getName() == null){
            return 0;
        }
        return (getName() + getClass().getSimpleName()).hashCode();
    }
}
